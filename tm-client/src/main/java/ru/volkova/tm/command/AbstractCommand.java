package ru.volkova.tm.command;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.service.CommandService;
import ru.volkova.tm.service.SessionService;

@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected CommandService commandService;
    
    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    @Autowired
    protected SessionService sessionService;

}
