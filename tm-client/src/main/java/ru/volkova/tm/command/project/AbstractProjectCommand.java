package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.endpoint.Project;
import ru.volkova.tm.endpoint.ProjectEndpoint;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    protected void showProject(@NotNull final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
    }

}
