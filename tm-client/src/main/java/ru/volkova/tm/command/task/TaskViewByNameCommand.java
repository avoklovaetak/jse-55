package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Task;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskViewByNameCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "find task by name";
    }

    @Override
    public void execute() {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final Task task = taskEndpoint.findTaskByName(session, name);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-name";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
