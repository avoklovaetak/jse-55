package ru.volkova.tm.command.bonds;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.endpoint.ProjectTaskEndpoint;

public abstract class AbstractProjectTaskClass extends AbstractCommand {

    @NotNull
    @Autowired
    protected ProjectTaskEndpoint projectTaskEndpoint;

}
