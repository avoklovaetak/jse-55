package ru.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.command.system.AboutCommand;
import ru.volkova.tm.endpoint.AdminUserEndpoint;

public abstract class AbstractUserCommand extends AboutCommand {

    @NotNull
    @Autowired
    public AdminUserEndpoint adminEndpoint;

}
