package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "clear all projects";
    }

    @Override
    public void execute() {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[PROJECT CLEAR]");
        @Nullable final Session session = sessionService.getSession();
        projectEndpoint.clearProject(session);
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
