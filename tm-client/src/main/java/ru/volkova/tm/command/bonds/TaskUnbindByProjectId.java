package ru.volkova.tm.command.bonds;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class TaskUnbindByProjectId extends AbstractProjectTaskClass {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "unbind task out of project";
    }

    @Override
    public void execute() {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @Nullable final Session session = sessionService.getSession();
        if (session == null) throw new ObjectNotFoundException();
        projectTaskEndpoint.unbindTaskByProjectId(session, projectId, taskId);
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
