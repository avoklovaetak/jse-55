package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.endpoint.AdminUserEndpoint;
import ru.volkova.tm.endpoint.TaskEndpoint;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.endpoint.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminEndpoint;

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus());
    }

}
