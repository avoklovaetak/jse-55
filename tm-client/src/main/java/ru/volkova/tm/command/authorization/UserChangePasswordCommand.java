package ru.volkova.tm.command.authorization;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.SessionGraph;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractAuthCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change password of user profile";
    }

    @Override
    @SneakyThrows
    public void execute() {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        userEndpoint.setPassword(session, password);
    }

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
