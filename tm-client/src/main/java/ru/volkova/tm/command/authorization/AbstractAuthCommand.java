package ru.volkova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.endpoint.AdminUserEndpoint;
import ru.volkova.tm.endpoint.SessionEndpoint;
import ru.volkova.tm.endpoint.UserEndpoint;

public abstract class AbstractAuthCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    public UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public AdminUserEndpoint adminUserEndpoint;

}
