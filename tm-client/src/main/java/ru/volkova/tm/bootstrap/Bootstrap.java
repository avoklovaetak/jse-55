package ru.volkova.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.endpoint.*;
import ru.volkova.tm.exception.empty.EmptyCommandException;
import ru.volkova.tm.exception.system.UnknownArgumentException;
import ru.volkova.tm.service.*;
import ru.volkova.tm.util.SystemUtil;
import ru.volkova.tm.util.TerminalUtil;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private CommandService commandService;

    @Autowired
    public AbstractCommand[] commands;

    @SneakyThrows
    private void initCommands() {
        Arrays.stream(commands).forEach(cmd -> commandService.add(cmd));
    }

    @SneakyThrows
    private void initPID() throws IOException {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void parseArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new UnknownArgumentException();
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyCommandException();
        @Nullable final AbstractCommand command = commandService.getCommandByName(name);
        if (command == null) return;
        command.execute();
    }

    public void run (final String... args) throws Exception {
        initPID();
        initCommands();
        @NotNull String command = "";
        if (parseArgs(args)) System.exit(0);
        while (!command.equals("exit")) {
            System.out.println("ENTER COMMAND:");
            command = TerminalUtil.nextLine();
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.err.println("[FAIL]");
            }
        }
    }

}
