package ru.volkova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.volkova.tm.endpoint.Session;

@Service
public class SessionService {

    @Nullable
    private Session session = null;

    @Nullable
    public Session getSession() {
        return session;
    }

    public void setSession(@Nullable Session session) {
        this.session = session;
    }

}