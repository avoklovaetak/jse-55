package ru.volkova.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.volkova.tm.api.repository.model.ITaskGraphRepository;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.ProjectGraph;
import ru.volkova.tm.model.TaskGraph;

import java.util.List;

@Repository
@Scope("prototype")
public class TaskGraphRepository extends AbstractRepository<TaskGraph> implements ITaskGraphRepository {

    @Override
    public void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.project.id = :projectId" +
                                "WHERE t.user.id = :userId AND t.id = :id ",
                        ProjectGraph.class)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void changeOneStatusById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable Status status
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.status = :status" +
                                "WHERE t.user.id = :userId AND t.id = :id",
                        TaskGraph.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setParameter("status", status)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void changeOneStatusByName(
            @NotNull String userId,
            @Nullable String name,
            @Nullable Status status
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.status = :status" +
                                "WHERE t.user.id = :userId AND t.name = :name",
                        TaskGraph.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setParameter("status", status)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void clear(@NotNull String userId) {
        entityManager
                .createQuery("DELETE FROM task t WHERE t.user.id = :userId")
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    @NotNull
    public List<TaskGraph> findAll(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT t FROM task t", TaskGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<TaskGraph> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                        "WHERE t.project.id = :projectId", TaskGraph.class)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @Nullable
    public TaskGraph findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                                "WHERE t.user.id = :user_id AND t.id = :id",
                        TaskGraph.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public TaskGraph findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        return entityManager
                .createQuery("SELECT t FROM task t WHERE t.user.id = :userId",
                        TaskGraph.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public TaskGraph findOneByName(@NotNull String userId, @NotNull String name) {
        return entityManager
                .createQuery("SELECT t FROM task t " +
                                "WHERE t.user.id = :userId AND t.name = :name",
                        TaskGraph.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public TaskGraph insert(@Nullable TaskGraph taskGraph) {
        if (taskGraph == null) throw new TaskNotFoundException();
        entityManager.persist(taskGraph);
        return taskGraph;
    }

    @Override
    public void removeAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        entityManager
                .createQuery("DELETE t FROM task t " +
                        "WHERE t.project.id = :projectId", TaskGraph.class)
                .setParameter("projectId", projectId)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        entityManager
                .createQuery("DELETE FROM task t " +
                        "WHERE t.user.id = :userId and t.id = :id", TaskGraph.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        entityManager
                .createQuery("DELETE FROM task t " +
                        "WHERE t.user.id = :userId and t.name = :name", TaskGraph.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void unbindTaskByProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        entityManager
                .createQuery("UPDATE task t SET t.project.id = :projectId" +
                                "WHERE t.user.id = :userId AND t.id = :id ",
                        ProjectGraph.class)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", null)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        entityManager
                .createQuery("UPDATE task t SET t.name = :name, t.description = :description" +
                                "WHERE t.user.id = :userId AND t.id = :id",
                        TaskGraph.class)
                .setParameter("name", name)
                .setParameter("description", description)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

}
