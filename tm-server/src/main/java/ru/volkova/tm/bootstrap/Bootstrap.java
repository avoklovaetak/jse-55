package ru.volkova.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.*;
import ru.volkova.tm.api.service.model.*;
import ru.volkova.tm.endpoint.*;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.service.ActiveMQConnectionService;
import ru.volkova.tm.service.LogService;
import ru.volkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private ILogService loggerService = new LogService();

    @NotNull
    @Autowired
    private IAdminUserGraphService adminUserService;

    @NotNull
    private IActiveMQConnectionService activeMQConnectionService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] abstractEndpoints;

    @SneakyThrows
    public void init() {
        initPID();
        initJMSBroker();
        initActiveMQConnectionService();
        initEndpoint();
        initUser();
    }

    private void initEndpoint() {
        Arrays.stream(abstractEndpoints).forEach(this::registry);
    }

    @SneakyThrows
    private void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initUser() {
        //if (adminUserService.findByLogin("test") != null) {
        adminUserService.createUser("test", "test");
        //}
        //if (adminUserService.findByLogin("test") != null) {
        adminUserService.createUserWithRole("test1", "test1", Role.ADMIN);
        //}
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initActiveMQConnectionService() {
        activeMQConnectionService = new ActiveMQConnectionService();
    }

    @SneakyThrows
    private void initJMSBroker() {
        @NotNull final BrokerService broker = new BrokerService();
        @NotNull final String bindAddress = "tcp://" + BrokerService.DEFAULT_BROKER_NAME + ":61615";
        broker.addConnector(bindAddress);
        broker.start();
    }

}
