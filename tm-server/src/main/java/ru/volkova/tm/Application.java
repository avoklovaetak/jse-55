package ru.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.configuration.ServerConfiguration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "yourRootElementName")
@XmlAccessorType(XmlAccessType.FIELD)

public class Application {

    public static void main(final String[] args) throws Exception {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
    }

}
