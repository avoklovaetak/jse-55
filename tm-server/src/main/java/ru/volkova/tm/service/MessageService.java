package ru.volkova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.service.IExecutorService;
import ru.volkova.tm.dto.Logger;
import ru.volkova.tm.enumerated.OperationType;

import javax.jms.*;

import static ru.volkova.tm.service.ActiveMQConnectionService.JMS_LOGGER_TOPIC;

@Service
@NoArgsConstructor
public class MessageService {

    @NotNull
    IExecutorService executorService = new ExecutorService();

    @NotNull
    @SneakyThrows
    private Logger getLogger(@NotNull final Object entity, @NotNull final OperationType operation) {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String className = entity.getClass().getSimpleName();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(entity);

        final Logger logger = new Logger();
        logger.setOperation(operation);
        logger.setEntity(json);
        logger.setClassName(className);

        return logger;
    }

    public void sendAsync(
            @NotNull final Object entity,
            @NotNull final OperationType operation
    ) {
        executorService.submit(() -> sendSync(entity, operation));
    }

    @SneakyThrows
    public void sendSync(
            @NotNull final Object entity,
            @NotNull final OperationType operation
    ) {
        @NotNull final Logger logger = getLogger(entity, operation);
        @NotNull final Connection connection = ActiveMQConnectionService.getInstance().getConnection();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(JMS_LOGGER_TOPIC);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final ObjectMessage objectMessage = session.createObjectMessage(logger);
        producer.send(objectMessage);
        producer.close();
        session.close();
    }

}
