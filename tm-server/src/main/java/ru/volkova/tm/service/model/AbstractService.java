package ru.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.service.model.IServiceGraph;
import ru.volkova.tm.model.AbstractEntityGraph;

@Service
public abstract class AbstractService<E extends AbstractEntityGraph> implements IServiceGraph<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
