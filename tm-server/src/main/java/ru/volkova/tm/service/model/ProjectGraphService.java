package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.model.IProjectGraphRepository;
import ru.volkova.tm.api.service.model.IAdminUserGraphService;
import ru.volkova.tm.api.service.model.IProjectGraphService;
import ru.volkova.tm.model.ProjectGraph;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;

@Service
public final class ProjectGraphService extends AbstractService<ProjectGraph> implements IProjectGraphService {

    @NotNull
    @Autowired
    private IAdminUserGraphService adminUserService;

    @Override
    public void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull ProjectGraph projectGraph = new ProjectGraph();
        projectGraph.setName(name);
        projectGraph.setDescription(description);
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        projectGraph.setUser(adminUserService.findById(userId));
        try {
            projectRepository.begin();
            projectRepository.insert(projectGraph);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            entities.forEach(projectRepository::insert);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void changeOneStatusById(@NotNull String userId, @NotNull String id, @Nullable Status status) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.changeOneStatusById(userId, id, status);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void changeOneStatusByName(@NotNull String userId, @NotNull String name, @Nullable Status status) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.changeOneStatusByName(userId, name, status);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.clear(userId);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectGraph> findAll(@NotNull String userId) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            final List<ProjectGraph> projectGraphs = projectRepository.findAll(userId);
            projectRepository.commit();
            return projectGraphs;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    public ProjectGraph findById(@NotNull String userId, @NotNull String id) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            final ProjectGraph projectGraph = projectRepository.findById(userId, id);
            projectRepository.commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    public ProjectGraph findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        if (index < 0) return null;
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            final ProjectGraph projectGraph = projectRepository.findOneByIndex(userId, index);
            projectRepository.commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    public ProjectGraph findOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            final ProjectGraph projectGraph = projectRepository.findOneByName(userId, name);
            projectRepository.commit();
            return projectGraph;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    public IProjectGraphRepository getProjectRepository() {
        return context.getBean(IProjectGraphRepository.class);
    }

    @SneakyThrows
    @Override
    public void insert(@Nullable final ProjectGraph projectGraph) {
        if (projectGraph == null) throw new ProjectNotFoundException();
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.insert(projectGraph);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeById(userId, id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByName(userId, name);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void updateOneById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.updateOneById(userId, id, name, description);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

}
