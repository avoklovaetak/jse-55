package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.model.ITaskGraphRepository;
import ru.volkova.tm.api.service.model.IAdminUserGraphService;
import ru.volkova.tm.api.service.model.ITaskGraphService;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.TaskGraph;

import java.util.List;

@Service
public final class TaskGraphService extends AbstractService<TaskGraph> implements ITaskGraphService {

    @NotNull
    @Autowired
    private IAdminUserGraphService adminUserService;

    @Override
    public void add(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull TaskGraph taskGraph = new TaskGraph();
        taskGraph.setName(name);
        taskGraph.setDescription(description);
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskGraph.setUser(adminUserService.findById(userId));
            taskRepository.begin();
            taskRepository.insert(taskGraph);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<TaskGraph> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            entities.forEach(taskRepository::insert);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void changeOneStatusById(
            @NotNull String userId, @NotNull String id, @Nullable Status status
    ) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.changeOneStatusById(userId, id, status);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void changeOneStatusByName(
            @NotNull String userId, @NotNull String name, @Nullable Status status
    ) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.changeOneStatusByName(userId, name, status);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.clear(userId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    public List<TaskGraph> findAll(@NotNull final String userId) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final List<TaskGraph> taskGraphs = taskRepository.findAll(userId);
            taskRepository.commit();
            return taskGraphs;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    public TaskGraph findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final TaskGraph taskGraph = taskRepository.findById(userId, id);
            taskRepository.commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    public TaskGraph findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) return null;
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final TaskGraph taskGraph = taskRepository.findOneByIndex(userId, index);
            taskRepository.commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    public TaskGraph findOneByName(@NotNull String userId, @NotNull String name) {
        if (name.isEmpty()) return null;
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final TaskGraph taskGraph = taskRepository.findOneByName(userId, name);
            taskRepository.commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    public ITaskGraphRepository getTaskRepository() {
        return context.getBean(ITaskGraphRepository.class);
    }

    @Override
    public TaskGraph insert(@NotNull final TaskGraph taskGraph) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.insert(taskGraph);
            taskRepository.commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.removeById(userId, id);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.removeOneByName(userId, name);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.updateOneById(userId, id, name, description);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}
