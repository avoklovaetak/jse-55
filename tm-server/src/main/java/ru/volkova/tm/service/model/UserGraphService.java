package ru.volkova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.model.IUserGraphRepository;
import ru.volkova.tm.api.service.model.IUserGraphService;
import ru.volkova.tm.model.UserGraph;

@Service
public final class UserGraphService extends AbstractService<UserGraph> implements IUserGraphService {

    @NotNull
    public IUserGraphRepository getUserRepository() {
        return context.getBean(IUserGraphRepository.class);
    }

    public void setPassword(
            @NotNull final String userId,
            @Nullable final String password
    ) {
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.setPassword(userId, password);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

}
