package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.model.IUserGraphRepository;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.model.IAdminUserGraphService;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.EmailExistsException;
import ru.volkova.tm.exception.auth.LoginExistsException;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.model.UserGraph;
import ru.volkova.tm.util.HashUtil;

import java.util.List;

@Service
public class AdminUserGraphService extends AbstractService<UserGraph> implements IAdminUserGraphService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @NotNull
    public UserGraph add(@Nullable final UserGraph userGraph) {
        if (userGraph == null) throw new UserNotFoundException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.insert(userGraph);
            userRepository.commit();
            return userGraph;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<UserGraph> entities) {
        if (entities == null) throw new UserNotFoundException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            entities.forEach(userRepository::insert);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.clear();
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @NotNull
    @Override
    public UserGraph createUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final UserGraph userGraph = new UserGraph();
        userGraph.setRole(Role.USER);
        userGraph.setLogin(login);
        userGraph.setPasswordHash(HashUtil.salt(propertyService, password));
        add(userGraph);
        return userGraph;
    }

    @NotNull
    @Override
    public UserGraph createUserWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        UserGraph userGraph = new UserGraph();
        userGraph.setEmail(email);
        final String passwordHash = HashUtil.salt(propertyService, password);
        userGraph.setPasswordHash(passwordHash);
        userGraph.setLogin(login);
        add(userGraph);
        return userGraph;
    }

    @NotNull
    @Override
    public UserGraph createUserWithRole
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        UserGraph userGraph = new UserGraph();
        userGraph.setPasswordHash(HashUtil.salt(propertyService, password));
        userGraph.setLogin(login);
        userGraph.setRole(role);
        add(userGraph);
        return userGraph;
    }

    @NotNull
    @Override
    public List<UserGraph> findAll() {
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final List<UserGraph> userGraphs = userRepository.findAll();
            userRepository.commit();
            return userGraphs;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Nullable
    public UserGraph findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final UserGraph userGraph = userRepository.findByEmail(email);
            userRepository.commit();
            return userGraph;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Nullable
    @Override
    public UserGraph findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final UserGraph userGraph = userRepository.findById(id);
            userRepository.commit();
            return userGraph;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    @Nullable
    public UserGraph findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final UserGraph userGraph = userRepository.findByLogin(login);
            userRepository.commit();
            return userGraph;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @NotNull
    public IUserGraphRepository getUserRepository() {
        return context.getBean(IUserGraphRepository.class);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @Nullable final UserGraph userGraph = findByEmail(email);
        return userGraph != null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @Nullable final UserGraph userGraph = findByLogin(login);
        return userGraph != null;
    }

    @Override
    public void lockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.lockByEmail(email);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void lockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.lockById(id);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.lockByLogin(login);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.removeByEmail(email);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.removeById(id);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.removeByLogin(login);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }


    @Override
    public void unlockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.unlockByEmail(email);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void unlockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.unlockById(id);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.unlockByLogin(login);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    ) {
        @Nullable final UserGraph userGraph = findById(userId);
        if (userGraph == null) throw new UserNotFoundException();
        if (firstName == null || firstName.isEmpty()
                || secondName == null || secondName.isEmpty()
                || middleName == null || middleName.isEmpty())
            throw new UserNotFoundException();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.updateUser(userId, firstName, secondName, middleName);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

}

