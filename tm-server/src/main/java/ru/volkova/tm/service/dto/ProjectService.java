package ru.volkova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.dto.IProjectRepository;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.api.service.dto.IProjectService;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IAdminUserService adminUserService;

    @Override
    public void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            project.setUser(adminUserService.findById(userId));
            projectRepository.begin();
            projectRepository.insert(project);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Project> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            entities.forEach(projectRepository::insert);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void changeOneStatusById(@NotNull String userId, @NotNull String id, @Nullable Status status) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.changeOneStatusById(userId, id, status);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void changeOneStatusByName(@NotNull String userId, @NotNull String name, @Nullable Status status) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.changeOneStatusByName(userId, name, status);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.clear(userId);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull String userId) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            final List<Project> projects = projectRepository.findAll(userId);
            projectRepository.commit();
            return projects;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    public Project findById(@NotNull String userId, @NotNull String id) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            final Project project = projectRepository.findById(userId, id);
            projectRepository.commit();
            return project;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull String userId, @NotNull Integer index) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        if (index < 0) return null;
        try {
            projectRepository.begin();
            final Project project = projectRepository.findOneByIndex(userId, index);
            projectRepository.commit();
            return project;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            final Project project = projectRepository.findOneByName(userId, name);
            projectRepository.commit();
            return project;
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @NotNull
    public IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @SneakyThrows
    @Override
    public void insert(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.insert(project);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeById(userId, id);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.removeOneByName(userId, name);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

    @Override
    public void updateOneById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description) {
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            projectRepository.begin();
            projectRepository.updateOneById(userId, id, name, description);
            projectRepository.commit();
        } catch (@NotNull final Exception e) {
            projectRepository.rollback();
            throw e;
        } finally {
            projectRepository.close();
        }
    }

}
