package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.volkova.tm.api.service.dto.IProjectTaskService;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @WebMethod
    public void bindTaskByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId", partName = "taskId") String taskId
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectTaskService.bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllTasksByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return projectTaskService.findAllTasksByProjectId(userId, projectId);
    }

    @WebMethod
    public void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectTaskService.removeProjectById(userId, id);
    }

    @WebMethod
    public void unbindTaskByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId", partName = "taskId") String taskId
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectTaskService.unbindTaskByProjectId(userId, projectId, taskId);
    }

}
