package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getCacheProviderConfig();

    @NotNull
    String getCacheRegionFactory();

    @NotNull
    String getCacheRegionPrefix();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDialect();

    @NotNull
    String getHbm2dllAuto();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getJdbcPassword();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcUser();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

    @NotNull
    String getShowSql();

    @NotNull
    String getUseLiteMemberValue();

    @NotNull
    String getUseMinimalPuts();

    @NotNull
    String getUseQueryCache();

    @NotNull
    String getUseSecondLevelCache();

}
