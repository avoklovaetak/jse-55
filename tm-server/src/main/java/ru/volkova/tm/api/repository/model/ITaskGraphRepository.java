package ru.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.model.TaskGraph;

import java.util.List;

public interface ITaskGraphRepository extends IRepositoryGraph<TaskGraph> {

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    void clear(@NotNull String userId);

    @NotNull
    List<TaskGraph> findAll(@NotNull String userId);

    @NotNull
    List<TaskGraph> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskGraph findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    TaskGraph findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskGraph findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    TaskGraph insert(@Nullable final TaskGraph taskGraph);

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void unbindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
