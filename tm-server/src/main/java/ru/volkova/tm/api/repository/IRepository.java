package ru.volkova.tm.api.repository;

import ru.volkova.tm.dto.AbstractEntity;

public interface IRepository<E extends AbstractEntity> {

    void begin();

    void close();

    void commit();

    void rollback();

}
