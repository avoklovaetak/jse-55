package ru.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void add(@NotNull Session session);

    boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    );

    @Nullable
    Session close(@Nullable Session session);

    @Nullable
    List<Session> findAll();

    @Nullable
    Session open(String login, String password);

    void validate(@Nullable final Session session);

    void validateAdmin(@Nullable final Session session, @Nullable final Role role);

}
