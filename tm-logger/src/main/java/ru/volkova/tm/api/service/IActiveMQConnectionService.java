package ru.volkova.tm.api.service;

import javax.jms.MessageListener;

public interface IActiveMQConnectionService {

    void receive(MessageListener listener);

}
